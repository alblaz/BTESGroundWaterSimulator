# Public API

## General 

```@docs
initialize
```

```@docs
simulate!
```

```@docs
BoreholeNetwork
```

```@docs
BoreholeOperation
```


## Simulation Options

```@docs
SimulationOptions
```

### Medium

```@docs
Medium
```

#### Options

```@docs
GroundMedium
```

```@docs
FlowInPorousMedium
```

### Borefield

```@docs
Borefield
```

#### Options

```@docs
EqualBoreholesBorefield
```

### Borehole

```@docs
Borehole
```

#### Options

```@docs
SingleUPipeBorehole
```

### Constraint

```@docs
Constraint
```

#### Options

```@docs
HeatLoadConstraint
```

```@docs
InletTempConstraint
```

### Time Superposition Method
```@docs
TimeSuperpositionMethod
```

#### Options

```@docs
ConvolutionMethod
```

```@docs
NonHistoryMethod
```

### Boundary Condition

```@docs
BoundaryCondition
```

#### Options
```@docs
NoBoundary
```

```@docs
DirichletBoundaryCondition
```
